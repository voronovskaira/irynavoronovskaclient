package locations;

import model.Location;
import model.User;
import org.testng.Assert;
import org.testng.annotations.Test;
import utils.ModelMapper;

import javax.ws.rs.core.Response;

public class TestUserLocation extends AbstractTest {

    @Test
    public void testCheckInForNotExistingUser() {
        User user = new User("Slavko", new Location());
        Response response = client.post("users/checkIn", ModelMapper.mapModelToJson(user));
        Assert.assertEquals(response.getStatus(), 404);
        Assert.assertEquals(response.readEntity(String.class), "Not found user with this name!");
    }

    @Test
    public void testCheckInButLocationNotFound() {
        User user = new User("Vova", new Location("Silence", "Bar"));
        Response response = client.post("users/checkIn", ModelMapper.mapModelToJson(user));
        Assert.assertEquals(response.getStatus(), 404);
        Assert.assertEquals(response.readEntity(String.class), "Not found location what you want to change for user");
    }

    @Test
    public void testCheckIn() {
        User user = new User("Vova", new Location("Power", "Museum"));
        Response response = client.post("users/checkIn", ModelMapper.mapModelToJson(user));
        Assert.assertEquals(response.getStatus(), 200);
    }
}
