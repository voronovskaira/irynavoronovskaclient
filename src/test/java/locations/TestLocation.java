package locations;

import org.testng.Assert;
import org.testng.annotations.Test;

import javax.ws.rs.core.Response;

public class TestLocation extends AbstractTest{

    @Test
    public void testGetAllLocations(){
        Response response = client.get("/locations/getAllLocations");
        Assert.assertEquals(response.getStatus(), 200);
    }

}
