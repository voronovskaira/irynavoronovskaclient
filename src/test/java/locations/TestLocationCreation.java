package locations;

import model.Location;
import org.testng.Assert;
import org.testng.annotations.Test;
import utils.ModelMapper;

import javax.ws.rs.core.Response;


public class TestLocationCreation extends AbstractTest {

    @Test
    public void testCreateLocation() {
        Location location = new Location("Hotel", "Barbaris", 1, 5);
        Response response = client.post("locations/createLocation", ModelMapper.mapModelToJson(location));
        Assert.assertEquals(response.getStatus(), 201);
        Location location1 = ModelMapper.mapJsonToLocation(response.readEntity(String.class));
        Assert.assertEquals(location, location1);
    }

    @Test
    public void testCreateLocationWithoutName() {
        Location location = new Location("Hotel", null, 1, 5);
        Response response = client.post("locations/createLocation", ModelMapper.mapModelToJson(location));
        Assert.assertEquals(response.getStatus(), 400);
        Assert.assertEquals(response.readEntity(String.class), "Please provide the location name !!");
    }

    @Test
    public void testEmptyLocation() {
        Location location = null;
        Response response = client.post("locations/createLocation", ModelMapper.mapModelToJson(location));
        Assert.assertEquals(response.getStatus(), 400);
        Assert.assertEquals(response.readEntity(String.class), "Please add location details !!");
    }

    @Test
    public void testCreateLocationWithNegativeCoordinate() {
        Location location = new Location("Bar", "Flower", -1, -5);
        Response response = client.post("locations/createLocation", ModelMapper.mapModelToJson(location));
        Assert.assertEquals(response.getStatus(), 400);
        Assert.assertEquals(response.readEntity(String.class), "Please specify correct location coordinate, it should be more then zero");
    }
}
