package model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class User extends Model {
    private String name;
    private Location location;

    public User(String name, Location location) {
        this.name = name;
        this.location = location;
    }

    public User() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", location=" + location +
                '}';
    }

}
