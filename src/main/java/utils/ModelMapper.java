package utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import model.Location;
import model.Model;
import model.User;

import java.io.IOException;

public class ModelMapper {

    public static String mapModelToJson(Model model) {
        String jsonLocation = "";
        ObjectMapper mapper = new ObjectMapper();
        try {
            jsonLocation = mapper.writeValueAsString(model);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return jsonLocation;
    }

    public static Location mapJsonToLocation(String string) {
        Location location = null;
        try {
            location = new ObjectMapper().readValue(string, Location.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return location;

    }

    public static User mapJsonToUser(String string) {
        User user = null;
        try {
            user = new ObjectMapper().readValue(string, User.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return user;

    }
}
