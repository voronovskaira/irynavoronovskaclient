package core;

import model.Location;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class WebClient {

    private String HOST = "http://localhost:8080/IrynaVoronovskaService";

    public Response get(String path){
        return ClientBuilder.newClient().target(HOST).path(path)
                .request(MediaType.APPLICATION_JSON)
                .get(Response.class);
    }

    public Response post(String path, String entity){
        return ClientBuilder.newClient().target(HOST).path(path)
                .request(MediaType.APPLICATION_JSON)
                .post(Entity.entity(entity, MediaType.APPLICATION_JSON_TYPE));
    }

}
